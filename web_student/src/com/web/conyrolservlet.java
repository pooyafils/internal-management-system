package com.web;

import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


/**
 * Servlet implementation class conyrolservlet
 */
@WebServlet("/conyrolservlet")
public class conyrolservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private dbutil dbutil;
	@Resource(name="jdbc/web_student_tracker")
	private DataSource dataSource;

	@Override
	public void init() throws ServletException {

		//dbutil= new dbutil(dataSource);
		super.init();
		try {
			dbutil=new dbutil(dataSource);
		}
		catch(Exception exc) {

		throw new ServletException(exc);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String theCommand=request.getParameter("command");
			if(theCommand==null) {
				theCommand="LIST";
				
			}
			switch(theCommand){
				case"LIST":
			liststudent(request,response);
			break;
				case"ADD":
				addStudent(request,response);
				break;
				case"LOAD":
					loadstudent(request,response);
					break;
				case "UPDATE":
					updateStudent(request, response);
					break;
				case "DELETE":
					deletestudent(request, response);
					break;
				default:
					liststudent(request,response);
					
			}
			
		}
		catch(Exception exc) {

			throw new ServletException(exc);
		}
		
	}
	private void deletestudent(HttpServletRequest request, HttpServletResponse response) throws Exception{
		// read student id from form data
				String theStudentId = request.getParameter("studentId");
				
				// delete student from database
					dbutil.deleteStudent(theStudentId);
				
				// send them back to "list students" page
				liststudent(request, response);
	}

	private void updateStudent(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

			// read student info from form data
			int id = Integer.parseInt(request.getParameter("studentId"));
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			
			// create a new student object
			student thestudent = new student(id, firstName, lastName, email);
			
			// perform update on database
			dbutil.updateStudent(thestudent);
			
			// send them back to the "list students" page
			liststudent(request, response);
			
		}


	private void loadstudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
			String theStudentId=request.getParameter("studentId");
			student thestudent=dbutil.getstudent(theStudentId);
			request.setAttribute("thestudent", thestudent);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/update-student-for.jsp");

			dispatcher.forward(request,response);
			
			
	}

	private void addStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String firstName=request.getParameter("firstName");
		String lastName=request.getParameter("lastName");
		String email=request.getParameter("email");
		student thestudent=new student(firstName,lastName,email); 
		dbutil.addstudent(thestudent);
		liststudent(request,response);

		
	}

	private void liststudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		List<student> student=dbutil.getstudent();
	request.setAttribute("studentlist",student);
	//fix
	RequestDispatcher dispatcher = request.getRequestDispatcher("/liststudent.jsp");

	dispatcher.forward(request,response);
	}

	

}
