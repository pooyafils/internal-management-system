package com.web;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;


public class dbutil {

	private DataSource dataSource;

	public dbutil(DataSource theDataSource) {
		dataSource = theDataSource;
	}
	
	public List<student> getstudent() throws Exception {
		
		List<student> students = new ArrayList<>();
		
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			// get a connection
			myConn = dataSource.getConnection();
			
			// create sql statement
			String sql = "select * from student order by last_name";
			//
			myStmt = myConn.createStatement();
			
			// execute query
			myRs = myStmt.executeQuery(sql);
			
			// process result set
			while (myRs.next()) {
////////				
				// retrieve data from result set row
				int id = myRs.getInt("id");
				String firstName = myRs.getString("first_name");
				String lastName = myRs.getString("last_name");
				String email = myRs.getString("email");
				
				// create new student object
				student tempstudent = new student(id, firstName, lastName, email);
///////				
				// add it to the list of students
				students.add(tempstudent);				
			}
			
			return students;		
		}
		finally {
			// close JDBC objects
			close(myConn, myStmt, myRs);
		}		
	}

	private void close(Connection myConn, Statement myStmt, ResultSet myRs) {

		try {
			if (myRs != null) {
				myRs.close();
			}
			
			if (myStmt != null) {
				myStmt.close();
			}
			
			if (myConn != null) {
				myConn.close();   // doesn't really close it ... just puts back in connection pool
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public void addstudent(student thestudent) throws SQLException {
		Connection myConn = null;
		PreparedStatement myStmt=null;
		/////////
		try {
			myConn = dataSource.getConnection();
			String sql="insert into student"
					+"(first_name,last_name,email)"
					+"value(?,?,?)";
			myStmt=myConn.prepareStatement(sql);

			myStmt.setString(1,thestudent.getFirstName());
			myStmt.setString(2,thestudent.getLastName());
			myStmt.setString(3,thestudent.getEmail());

			myStmt.execute();

		}
		finally {
			close(myConn,myStmt,null);
		}
	}

	public student getstudent(String theStudentId) throws Exception {	
		student thestudent=null;
		Connection myConn = null;
		PreparedStatement myStmt=null;
		ResultSet myRs = null;
		int studentId;
		String pooya;
try {

		studentId=Integer.parseInt(theStudentId);
	myConn = dataSource.getConnection();
	String sql="select* from student where id=?";
	myStmt=myConn.prepareStatement(sql);
	myStmt.setInt(1, studentId);
		myRs=myStmt.executeQuery();
		if (myRs.next()) {
			String firstName = myRs.getString("first_name");
			String lastName = myRs.getString("last_name");
			String email = myRs.getString("email");
			
			// use the studentId during construction
			thestudent = new student(studentId, firstName, lastName, email);
		}
		else {
			throw new Exception("Could not find student id: " + studentId);
		}			
		
	return thestudent;
}
	finally {
		close(myConn,myStmt,null);

	}
		
		
	}
public void updateStudent(student thestudent) throws Exception {
		
		Connection myConn = null;
		PreparedStatement myStmt = null;

		try {
			// get db connection
			myConn = dataSource.getConnection();
			
			// create SQL update statement
			String sql = "update student "
						+ "set first_name=?, last_name=?, email=? "
						+ "where id=?";
			
			// prepare statement
			myStmt = myConn.prepareStatement(sql);
			
			// set params
			myStmt.setString(1, thestudent.getFirstName());
			myStmt.setString(2, thestudent.getLastName());
			myStmt.setString(3, thestudent.getEmail());
			myStmt.setInt(4, thestudent.getId());
			
			// execute SQL statement
			myStmt.execute();
		}
		finally {
			// clean up JDBC objects
			close(myConn, myStmt, null);
		}

}

public void deleteStudent(String theStudentId) throws Exception {
	Connection myConn = null;
	PreparedStatement myStmt = null;
	
	try {
		// convert student id to int
		int studentId = Integer.parseInt(theStudentId);
		
		// get connection to database
		myConn = dataSource.getConnection();
		
		// create sql to delete student
		String sql = "delete from student where id=?";
		
		// prepare statement
		myStmt = myConn.prepareStatement(sql);
		
		// set params
		myStmt.setInt(1, studentId);
		
		// execute sql statement
		myStmt.execute();
	}
	finally {
		// clean up JDBC code
		close(myConn, myStmt, null);
		

}
}
}
















