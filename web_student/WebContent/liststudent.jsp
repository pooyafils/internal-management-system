<%-- <%@ page import="java.util.*" %> --%>
<%-- <%@ page import="com.web.*" %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
	<title>Student Tracker App</title>	
	<link type="text/css" rel="stylesheet" href="css/style.css">
</head>

<%-- <% --%>
<!-- 	// get the students from the request object (sent by servlet) -->
<%-- 	List<student> theStudents = (List<student>) request.getAttribute("studentlist");%> --%>

<body>
<%
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("pragma","no-cache");
response.setHeader("Expire","0");



response.setHeader("Expires", "0");
if(session.getAttribute("username")==null){
	response.sendRedirect("login.jsp");
}
%>
${username}<br>
<form action="logout">
<input type="submit" value="logout">
</form>
	<div id="wrapper">
		<div id="header">
			<h2>FooBar University</h2>
		</div>
	</div>

	<div id="container">
	
		<div id="content">
		<input type="button" value="Add Student"
		onclick="window.location.href='add-student-fo.jsp';return false;"
		class="add-student-button"
		/>
			<table>
			
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
				
<%-- 				<% for (student tempStudent : theStudents) { %> --%>
<!-- 					<tr> -->
<%-- 						<td> <%= tempstudent.getEmail() %> </td> --%>
<%-- 						<td> <%= tempstudent.getLastName() %> </td> --%>
<%-- 						<td> <%= tempstudent.getEmail() %> </td> --%>
<!-- 					</tr> -->
				<c:forEach var="tempstudent" items="${studentlist}">
				<c:url var="templink" value="conyrolservlet">
				<c:param name="command" value="LOAD"/>
				<c:param name="studentId" value="${tempstudent.id}"/>
				</c:url>
			<c:url var="delete" value="conyrolservlet">
			<c:param name="command" value="DELETE"/>
			<c:param name="studentId" value="${tempstudent.id}"/>
			
			</c:url>
				
					<tr>
						<td>${tempstudent.firstName}</td>
						<td> ${tempstudent.lastName} </td>
						<td> ${tempstudent.email} </td>
						<td><a href="${templink}">Update</a>
												|
						<a href="${delete}"
						onclick="if (!(confirm('Are you sure you want to delete this student?'))) return false">
						
						DELETE</a>								
								
						</td>
					</tr>
				</c:forEach>
<%-- 				<% } %> --%>
				
			</table>
		
		</div>
	
	</div>
</body>


</html>


