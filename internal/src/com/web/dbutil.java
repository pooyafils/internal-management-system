package com.web;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;


public class dbutil {

	private DataSource dataSource;

	public dbutil(DataSource theDataSource) {
		dataSource = theDataSource;
	}
	
	public List<student> getstudent() throws Exception {
		
		List<student> students = new ArrayList<>();
		
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			// get a connection
			myConn = dataSource.getConnection();
			
			// create sql statement
			String sql = "select * from student order by last_name";
			//
			myStmt = myConn.createStatement();
			
			// execute query
			myRs = myStmt.executeQuery(sql);
			
			// process result set
			while (myRs.next()) {
////////				
				// retrieve data from result set row
				int id = myRs.getInt("id");
				String firstName = myRs.getString("first_name");
				String lastName = myRs.getString("last_name");
				String email = myRs.getString("email");
				String studentClass=myRs.getString("student_class");
				int studentId=myRs.getInt("student_id");
				// create new student object
				student tempstudent = new student(id, firstName, lastName, email, studentId,studentClass);
///////				
				// add it to the list of students
				students.add(tempstudent);				
			}
			
			return students;		
		}
		finally {
			// close JDBC objects
			close(myConn, myStmt, myRs);
		}		
	}

	private void close(Connection myConn, Statement myStmt, ResultSet myRs) {

		try {
			if (myRs != null) {
				myRs.close();
			}
			
			if (myStmt != null) {
				myStmt.close();
			}
			
			if (myConn != null) {
				myConn.close();   // doesn't really close it ... just puts back in connection pool
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public void addstudent(student thestudent) throws SQLException {
		Connection myConn = null;
		PreparedStatement myStmt=null;
		/////////
		try {
			myConn = dataSource.getConnection();
			String sql="insert into student"
					+"(first_name,last_name,email,student_id,student_class)"
					+"value(?,?,?,?,?)";
			myStmt=myConn.prepareStatement(sql);

			myStmt.setString(1,thestudent.getFirstName());
			myStmt.setString(2,thestudent.getLastName());
			myStmt.setString(3,thestudent.getEmail());
			myStmt.setInt(4,thestudent.getStudentId());
			myStmt.setString(5,thestudent.getStudentClass());
			myStmt.execute();

		}
		finally {
			close(myConn,myStmt,null);
		}
	}

	public student getstudent(String theStudentId) throws Exception {	
		student thestudent=null;
		Connection myConn = null;
		PreparedStatement myStmt=null;
		ResultSet myRs = null;
		int studentId;
		String pooya;
try {
			studentId=Integer.parseInt(theStudentId);
	myConn = dataSource.getConnection();
	String sql="select* from student where id=?";
	myStmt=myConn.prepareStatement(sql);
	myStmt.setInt(1, studentId);
		myRs=myStmt.executeQuery();
		if (myRs.next()) {
			String firstName = myRs.getString("first_name");
			String lastName = myRs.getString("last_name");
			String email = myRs.getString("email");
			String studentClass=myRs.getString("student_class");
			int studentIdone=myRs.getInt("student_id");
			// use the studentId during construction
			thestudent = new student(studentId, firstName, lastName, email, studentIdone,studentClass);
		}
		else {
			throw new Exception("Could not find student id: " + studentId);
		}			
		
	return thestudent;
}
	finally {
		close(myConn,myStmt,null);

	}
		
		
	}
public void updateStudent(student thestudent) throws Exception {
		
		Connection myConn = null;
		PreparedStatement myStmt = null;

		try {
			// get db connection
			myConn = dataSource.getConnection();
			
			// create SQL update statement
			String sql = "update student "
						+ "set first_name=?, last_name=?, email=? ,student_id=?,student_class=?"
						+ "where id=?";
			
			// prepare statement
			myStmt = myConn.prepareStatement(sql);
			
			// set params
			myStmt.setString(1, thestudent.getFirstName());
			myStmt.setString(2, thestudent.getLastName());
			myStmt.setString(3, thestudent.getEmail());
			myStmt.setInt(4, thestudent.getStudentId());
			myStmt.setString(5, thestudent.getStudentClass());

			myStmt.setInt(6, thestudent.getId());
			
			// execute SQL statement
			myStmt.execute();
		}
		finally {
			// clean up JDBC objects
			close(myConn, myStmt, null);
		}

}

public void deleteStudent(String theStudentId) throws Exception {
	Connection myConn = null;
	PreparedStatement myStmt = null;
	
	try {
		// convert student id to int
		int studentId = Integer.parseInt(theStudentId);
		
		// get connection to database
		myConn = dataSource.getConnection();
		
		// create sql to delete student
		String sql = "delete from student where id=?";
		
		// prepare statement
		myStmt = myConn.prepareStatement(sql);
		
		// set params
		myStmt.setInt(1, studentId);
		
		// execute sql statement
		myStmt.execute();
	}
	finally {
		// clean up JDBC code
		close(myConn, myStmt, null);
		

}
}	 

public List<student> getteacher() throws Exception {
	
	List<student> students = new ArrayList<>();
	
	Connection myConn = null;
	Statement myStmt = null;
	ResultSet myRs = null;
	
	try {
		// get a connection
		myConn = dataSource.getConnection();
		
		// create sql statement
		String sql = "select * from teacher order by id";
		//String sql="SELECT T.UNAME AS TEACHER , C.NAME AS CLASS\r\n" + 
		//		"FROM TEACHER T\r\n" + 
			//	"INNER JOIN TEACHER_CLASS TC ON TC.ID_TEACHER = T.ID\r\n" + 
			//	"INNER JOIN CLASS C ON C.ID = TC.ID_CLASS";
		myStmt = myConn.createStatement();
		
		// execute query
		myRs = myStmt.executeQuery(sql);
		
		// process result set
		while (myRs.next()) {
////////			
//			// retrieve data from result set row
//			int id = myRs.getInt("id");
//			String firstName = myRs.getString("first_name");
//			String lastName = myRs.getString("last_name");
//			String email = myRs.getString("email");
//			String studentClass=myRs.getString("student_class");
//			int studentId=myRs.getInt("student_id");
//			// create new student object
//			student tempstudent = new student(id, firstName, lastName, email, studentId,studentClass);
///////				
			int idteacher = myRs.getInt("id");
		String uname = myRs.getString("uname");
		String pass = myRs.getString("pass");
	//	int  idclass=myRs.getInt("id");
	//	String name=myRs.getString("name");
		student tempstudent = new student(idteacher,uname,pass);
		//student tempstudent = new student(idteacher,uname,pass,idclass,name);

			// add it to the list of students
			students.add(tempstudent);				
		}
		
		return students;		
	}
	finally {
		// close JDBC objects
		close(myConn, myStmt, myRs);
	}		
}

private void closeone(Connection myConn, Statement myStmt, ResultSet myRs) {

	try {
		if (myRs != null) {
			myRs.close();
		}
		
		if (myStmt != null) {
			myStmt.close();
		}
		
		if (myConn != null) {
			myConn.close();   // doesn't really close it ... just puts back in connection pool
		}
	}
	catch (Exception exc) {
		exc.printStackTrace();
	}
}

public student getteacher(String theidteacher) throws Exception {	//String theStudentId
	student thestudent=null;
	Connection myConn = null;
	PreparedStatement myStmt=null;
	ResultSet myRs = null;
	int studentId;
	int idteacherone;
	//String pooya;
try {

	//studentId=Integer.parseInt(theStudentId);
	idteacherone=Integer.parseInt(theidteacher);
myConn = dataSource.getConnection();
//String sql="SELECT T.UNAME AS TEACHER , C.NAME AS CLASS\r\n" + 
	//	"FROM TEACHER T\r\n" + 
	//	"INNER JOIN TEACHER_CLASS TC ON TC.ID_TEACHER = T.ID\r\n" + 
	//	"INNER JOIN CLASS C ON C.ID = TC.ID_CLASS";
String sql="select* from teacher where id=?";

myStmt=myConn.prepareStatement(sql);
myStmt.setInt(1, idteacherone);
	myRs=myStmt.executeQuery();
	if (myRs.next()) {
//		String firstName = myRs.getString("first_name");
//		String lastName = myRs.getString("last_name");
//		String email = myRs.getString("email");
//		String studentClass=myRs.getString("student_class");
//		int studentIdone=myRs.getInt("student_id");
//		// use the studentId during construction
//		thestudent = new student(studentId, firstName, lastName, email, studentIdone,studentClass);
		int idteacher = myRs.getInt("id");
		String uname = myRs.getString("uname");
		String pass = myRs.getString("pass");
	//	int  idclass=myRs.getInt("id");
	//	String name=myRs.getString("name");
		//student tempstudent = new student(idteacher,uname,pass,idclass,name);
		student tempstudent = new student(idteacher,uname,pass);
	}
	else {
		throw new Exception("Could not find student id: " + idteacherone);
	}			
	
return thestudent;
}
finally {
	close(myConn,myStmt,null);

}
	
	
}
public List <student> getcourse() throws Exception{
	List<student> course=new ArrayList<>();
	Connection myConn = null;
	Statement myStmt = null;
	ResultSet myRs = null;
	try {
		
		myConn=dataSource.getConnection();
		String sql="select id from class";
		myStmt=myConn.createStatement();
		myRs=myStmt.executeQuery(sql);
		while (myRs.next()) {
	int id = myRs.getInt("id");
	student tempstudent = new student(id);

				course.add(tempstudent);
	}
	return course;
}
	finally {
		// close JDBC objects
		close(myConn, myStmt, myRs);
	}		
}

public void insetcourse(student thestudent)throws SQLException  {
	Connection myConn = null;
	PreparedStatement myStmt=null;
	/////////
	try {
		myConn = dataSource.getConnection();
		String sql="insert into test"+"(id)"+"value(?)";
		myStmt=myConn.prepareStatement(sql);

		myStmt.setInt(1,thestudent.getId());
		
		myStmt.execute();
		
	}
	finally {
		close(myConn,myStmt,null);
	}
}
}

