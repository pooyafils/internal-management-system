package com.web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
public class teacher_class_dbutil {
	private DataSource dataSource;
	public teacher_class_dbutil(DataSource theDataSource) {
		dataSource = theDataSource;
	}
public List<student> getstudent() throws Exception {
		List<student> students = new ArrayList<>();
	//String connectionURL = "jdbc:mysql://localhost:3306/pooya";
	  String connectionURL = "jdbc:mysql://localhost:3306/web_student_tracker";
	  System.out.println("loding the driver");
		  Connection connection= null;
		  Statement s=null;
		  ResultSet rs = null;
		  try {
				  Class.forName("com.mysql.jdbc.Driver");
				//connection = DriverManager.getConnection(connectionURL, "root", ""); 
		          connection = DriverManager.getConnection(connectionURL, "pooya", ""); 
		    	  System.out.println("username and password is correect");
			      // String sql = "SELECT * from class ";
		        //  String sql="select classname  from classacess inner join teacher on classacess.teacherid=teacher.id";
		          String sql="select teacheracess.teacherid,teacheracess.id, teacheracess.classname, teacher.uname, teacher.pass FROM teacheracess inner join teacher on teacheracess.teacherid=teacher.id"; 
				 // String sqltwo="select teacherid from classacess";
		          s = connection.createStatement();
				   s.executeQuery (sql
						   );
				   rs = s.getResultSet();
					  System.out.println("excuting a sql quary");

				  while (rs.next ()){
					// int idclass=rs.getInt("id");
					//  String classname=rs.getString("name");
					//  student tempstudent=new student(idclass,classname);
					int teacherid=rs.getInt("teacherid");
					int id=rs.getInt("id");
					  String classname=rs.getString("classname");
					  String uname=rs.getString("uname");
					  String pass=rs.getString("pass");
						 //student tempstudent=new student(classname);
					  student tempstudent=new student(id, teacherid,classname,uname,pass);
					students.add(tempstudent);

				  }
					return students;
		  }
			finally {
				// close JDBC objects
				close(connection, s, rs);
		  }
  }
private void close(Connection connection, Statement s, ResultSet rs) {

	try {
		if (rs != null) {
			rs.close();
		}
		if (s != null) {
			s.close();
		}
		if (connection != null) {
			connection.close();   
		}
	}
	catch (Exception e) {
		throw new RuntimeException(e);
	}
}

public void delete (String theteacherid) throws Exception {
	Connection myConn = null;
	PreparedStatement myStmt = null;
	
	try {
		// convert student id to int
		String  teacherId =theteacherid;
		
		// get connection to database
		myConn = dataSource.getConnection();
		
		// create sql to delete student
		String sql = "delete from teacheracess where id=?	";	
		///String sql = "delete from classacess where id_class=?	";	

		
		// prepare statement
		myStmt = myConn.prepareStatement(sql);
		
		// set params
		myStmt.setString(1, teacherId);
		
		// execute sql statement
		myStmt.execute();
	}
	finally {
		// clean up JDBC code
		close(myConn, myStmt, null);
		

}
	
}
}

