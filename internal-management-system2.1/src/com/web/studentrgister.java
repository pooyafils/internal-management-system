package com.web;

public class studentrgister {
	private String name;
	private String fname;
	private String user;
	private String pass;
	private String email;
	private String studentClass;
	
	public studentrgister(String name, String fname, String user, String pass, String email) {
		super();
		this.name = name;
		this.fname = fname;
		this.user = user;
		this.pass = pass;
		this.email = email;
	}
	public studentrgister(String pass, String user) {
		this.user = user;
		this.pass = pass;
	}
	public studentrgister(String name, String fname, String email, String studentClass) {
		super();
		this.name = name;
		this.fname = fname;
		this.studentClass=studentClass;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStudentClass() {
		return studentClass;
	}
	public void setStudentClass(String studentClass) {
		this.studentClass = studentClass;
	}
	
}
