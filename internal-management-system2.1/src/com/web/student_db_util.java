package com.web;

import java.sql.Connection;	
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

public class student_db_util {
	private DataSource dataSource;

	public student_db_util(DataSource theDataSource) {
		dataSource = theDataSource;
	}
	private void close(Connection myConn, Statement myStmt, ResultSet myRs) {

		try {
			if (myRs != null) {
				myRs.close();
			}
			
			if (myStmt != null) {
				myStmt.close();
			}
			
			if (myConn != null) {
				myConn.close();   // doesn't really close it ... just puts back in connection pool
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	public void registerstudent(studentrgister student)throws SQLException {
		Connection myConn = null;
		PreparedStatement myStmt=null;
	try {
		myConn = dataSource.getConnection();
		String sql="insert into student "+"(first_name,last_name,email,user,pass,student_id,student_class)"+"value(?,?,?,?,?,?,?)";
		 myStmt=myConn.prepareStatement(sql);
		 myStmt.setString(1, student.getName());
		 myStmt.setString(2, student.getFname());
		 myStmt.setString(3, student.getEmail());
		 myStmt.setString(4, student.getUser());
		 myStmt.setString(5, student.getPass());
		 myStmt.setInt(6, 0);
		 myStmt.setString(7, "");
		 myStmt.execute();
		 
	}
	finally {
		close(myConn,myStmt,null);
	}
	
	}
	public boolean check(String user,String pass) throws SQLException {
		Connection myConn = null;
		PreparedStatement myStmt=null;
		boolean status=false;  

		try {
			myConn=dataSource.getConnection();
			String sql="select * from student where user=? and pass=?";
			myStmt=myConn.prepareStatement(sql);
			myStmt.setString(1, user);
			myStmt.setString(2, pass);
			ResultSet rs=myStmt.executeQuery();
			 status = rs.next();  

					
			
		}
		catch(Exception e) {
			e.printStackTrace();
			
		}
		return status;
	}
	//public studentrgister showinfo(String users,String passs) throws Exception  {
		public List <studentrgister> showinfo(String users,String passs) throws Exception  {
			List<studentrgister> students = new ArrayList<>();

		studentrgister mystudent=null;
		Connection myConn = null;
		PreparedStatement myStmt=null;
		ResultSet myRs = null;
		String  user=users;
		String pass=passs;
try {
	myConn = dataSource.getConnection();
	String sql="select* from student where user=? and pass=?";
	myStmt=myConn.prepareStatement(sql);
	myStmt.setString(1, user);
	myStmt.setString(2, pass);

		myRs=myStmt.executeQuery();
		if (myRs.next()) {
			String name = myRs.getString("first_name");
			String fname = myRs.getString("last_name");
			String email = myRs.getString("email");
			String studentClass=myRs.getString("student_class");
			

			// use the studentId during construction
		mystudent = new studentrgister(name, fname, email,studentClass);
		students.add(mystudent);
		}
		else {
			throw new Exception("Could not find student id: " );
		}			
		
		
		
		
		return students;
}
finally {
	close(myConn,myStmt,null);

}
	}

	
	
}
