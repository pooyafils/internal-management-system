package com.web;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class add_course
 */
@WebServlet("/add_course")
public class add_course extends HttpServlet {
	private dbutil dbutil;
	@Resource(name="jdbc/web_student_tracker")
	private DataSource dataSource;
	@Override
	public void init() throws ServletException {
		//dbutil= new dbutil(dataSource);
		super.init();
		try {
			dbutil=new dbutil(dataSource);
		}
		catch(Exception exc) {
		throw new ServletException(exc);
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		List<student> student;
//		try {
//			student = dbutil.getcourse();
//			request.setAttribute("select",student);
//			RequestDispatcher dispatcher = request.getRequestDispatcher("/course.jsp");
//		dispatcher.forward(request,response);
//		} catch (Exception e) {			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}


		try {
			String thecommand=request.getParameter("command");
			if(thecommand==null) {
				thecommand="LIST";
			}
			switch(thecommand) {
			case"LIST":
				listcourse(request,response);
				break;
			case"insert":
				insertcourse(request,response);
				break;
			case"load":
			loadstudentcourse(request,response);
			break;
			}
		}
		catch(Exception exc) {
			throw new ServletException(exc);
		}
		
	
	}
	private void insertcourse(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String firstName=request.getParameter("firstName");
		String lastName=request.getParameter("lastName");
		
		int courseid = Integer.parseInt(request.getParameter("select"));

		course thestudent=new course(courseid,firstName,lastName); 
		dbutil.insetcourse(thestudent);
		///  request.setAttribute("message", "Records loaded successfully");
		  RequestDispatcher dispatcher = request.getRequestDispatcher("/about.jsp");
		  dispatcher.forward(request,response); 

	}
	private void listcourse(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<course> student=dbutil.getcourse();
	request.setAttribute("select",student);
	RequestDispatcher dispatcher = request.getRequestDispatcher("/course.jsp");
	dispatcher.forward(request,response);
	}
	private void loadstudentcourse(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String theStudentId=request.getParameter("studentId");
		course thestudent=dbutil.getstudentcourse(theStudentId);
		request.setAttribute("thestudent", thestudent);
		List<course> student=dbutil.getcourse();
		request.setAttribute("select",student);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/course.jsp");
		dispatcher.forward(request,response);
}

}
