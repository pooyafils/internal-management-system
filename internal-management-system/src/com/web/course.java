package com.web;

public class course {
private int id;
private String name;
private int studentId;
private String firstName;
private String lastName;

public course(int id, String name) {
	super();
	this.id = id;
	this.name = name;
	
}

public course(int id, String firstName, String lastName) {
this.firstName=firstName;
this.lastName=lastName;
this.id=id;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getStudentId() {
	return studentId;
}
public void setStudentId(int studentId) {
	this.studentId = studentId;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}


}
