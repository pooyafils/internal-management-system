-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2020 at 12:23 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_student_tracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `uname` varchar(11) NOT NULL,
  `pass` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`uname`, `pass`) VALUES
('pass', 'pass');

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `name`) VALUES
(1, 'MATH'),
(2, 'PH'),
(3, 'c++'),
(4, 'java'),
(5, 'c#');

-- --------------------------------------------------------

--
-- Table structure for table `classacess`
--

CREATE TABLE `classacess` (
  `id` int(20) NOT NULL,
  `classname` varchar(30) NOT NULL,
  `teacherid` int(20) NOT NULL,
  `id_class` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classacess`
--

INSERT INTO `classacess` (`id`, `classname`, `teacherid`, `id_class`) VALUES
(1, 'java', 1, 4),
(2, 'ph', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mytest`
--

CREATE TABLE `mytest` (
  `id` int(11) DEFAULT NULL,
  `firstName` varchar(11) NOT NULL,
  `lastName` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mytest`
--

INSERT INTO `mytest` (`id`, `firstName`, `lastName`) VALUES
(1, 'Bill', 'Neely'),
(8, 'Nill', 'Neely'),
(1, 'po', 'fils'),
(2, 'Bill', 'Neely'),
(1, 'alex', ''),
(2, 'alex', ''),
(3, 'alex', '');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `student_id` varchar(10) NOT NULL,
  `student_class` varchar(20) NOT NULL,
  `user` varchar(11) NOT NULL,
  `pass` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `first_name`, `last_name`, `email`, `student_id`, `student_class`, `user`, `pass`) VALUES
(1, 'Mary', 'Public', 'mary@code.com', '1', 'A', 'm', 'm'),
(4, 'Bill', 'Neely', 'bill@code.com', '4', 'b', 'admin', 'admin'),
(5, 'Maxwell', 'Dixon', 'max@.com', '5', 'JAVA', '', ''),
(6, 'rio', 'ty', '5uu5', '6', 'java', '', ''),
(8, 'tony', '', '@sara', '8', 'java', '', ''),
(9, 'papco', 'hetrtrt', 'rggh', '9', 'A', '', ''),
(10, 'pet', 'm', '@code', '10', 'java', '', ''),
(11, 'one', 'one', '@onetocode.com', '11', 'c++', '', ''),
(12, 'popo', 'kk', 'ppoo', '43', 'A', '', ''),
(20, 'rr', NULL, 'fde', '0', '', 'cddd', 'fdxx'),
(21, 'alex', NULL, 'alex1100', '0', '', '2000', '2000'),
(22, 'yoyo', NULL, 'qq', '0', '', '@q.com', 'qq');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `uname` varchar(250) DEFAULT NULL,
  `pass` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `uname`, `pass`) VALUES
(1, 'PASS', 'PASS'),
(2, 'alex', 'macdonal');

-- --------------------------------------------------------

--
-- Table structure for table `teacheracess`
--

CREATE TABLE `teacheracess` (
  `id` int(11) NOT NULL,
  `classname` varchar(255) NOT NULL,
  `teacherid` int(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacheracess`
--

INSERT INTO `teacheracess` (`id`, `classname`, `teacherid`) VALUES
(1, 'java', 1),
(2, 'ph', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classacess`
--
ALTER TABLE `classacess`
  ADD KEY `classacess_ibfk_1` (`id_class`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacheracess`
--
ALTER TABLE `teacheracess`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `classacess`
--
ALTER TABLE `classacess`
  ADD CONSTRAINT `classacess_ibfk_1` FOREIGN KEY (`id_class`) REFERENCES `class` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
