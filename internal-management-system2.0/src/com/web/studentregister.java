package com.web;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 * Servlet implementation class studentregister
 */
@WebServlet("/studentregister")
public class studentregister extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private student_db_util student_db_util ;
	@Resource(name="jdbc/web_student_tracker")
	private DataSource dataSource;
	@Override
	public void init() throws ServletException {
		try {
			student_db_util =new student_db_util (dataSource);
		}
		catch(Exception exc) {

		throw new ServletException(exc);
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user=request.getParameter("user");
		String pass=request.getParameter("pass");
		student_db_util  da=new student_db_util (dataSource);
	//	if(user.equals("pooya")&& pass.equals("pooya")) {

		try {
			if(da.check(user, pass)) {
				HttpSession session=request.getSession();
				session.setAttribute("username", user);
				
			response.sendRedirect("student-dashboard.jsp");
			}
			else {
				response.sendRedirect("loginstudent.jsp");

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	


		try {
			String thecommand=request.getParameter("command");
			if(thecommand==null) {
				thecommand="LIST";
				switch(thecommand) {
				case"LIST":
					addStudent(request,response);
					break;
				
				}
				
			}
		}
			
			catch(Exception exc) {
				throw new ServletException(exc);
			}
		
	}
	private void addStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		String name=request.getParameter("name");
		String fname=request.getParameter("fame");
		String email=request.getParameter("email");
		String pass=request.getParameter("pass");
		String user=request.getParameter("user");
		studentrgister thestudent=new studentrgister(name,fname,email,pass,user);
	
			student_db_util.registerstudent(thestudent);
		//	RequestDispatcher dispatcher = request.getRequestDispatcher("/about.jsp");
			//addStudent(request,response);
			  RequestDispatcher dispatcher = request.getRequestDispatcher("/about.jsp");
			  dispatcher.forward(request,response); 

		
	}
	
	}


