package com.web;

public class student {
	private int id;
	private String firstName;
	private String lastName;
	private String uname;
	private String pass;
private int idteacher;
  private int classid;
  private String name;
	private String email;
	private int studentId;
	private String studentClass;
	private String classname;
	private int teacherid;
	public student(String classname,String uname, String pass) {
		super();
		this.classname=classname;
		this.uname = uname;
	this.pass = pass;
	

	}
	public student(int classid, String name) {
		super();
		this.classid = classid;
		this.name = name;
	}
	public student(int idteacher,String uname, String pass) {
		super();
		this.uname = uname;
	this.pass = pass;
this.idteacher=idteacher;
}
	//public student(int idteacher,String uname, String pass,int classid, String name) {
	//	super();
	//	this.uname = uname;
	//	this.pass = pass;
		//this.idteacher=idteacher;
	//	this.classid = classid;
		//this.name = name;
	//}
	
	public student(String firstName, String lastName, String email,int studentId,String studentClass) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.studentId=studentId;
		this.studentClass=studentClass;
	}

	public student(int id, String firstName, String lastName, String email,int studentId,String studentClass) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.studentId=studentId;
		this.studentClass=studentClass;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentClass() {
		return studentClass;
	}

	public void setStudentClass(String studentClass) {
		this.studentClass = studentClass;
	}
	
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public int getIdteacher() {
		return idteacher;
	}
	public void setIdteacher(int idteacher) {
		this.idteacher = idteacher;
		
	}
	public int getClassid() {
		return classid;
	}
	public void setClassid(int classid) {
		this.classid = classid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getClassname() {
		return classname;
	}
	public void setClassname(String classname) {
		this.classname = classname;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}	
}
