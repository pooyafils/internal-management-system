package com.web;
import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
@WebServlet("/tavle")
public class tavle extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private teacher_class_dbutil teacher_class_dbutil;
	@Resource(name="jdbc/web_student_tracker")
	private DataSource dataSource;
	@Override
	public void init() throws ServletException {
		try {
			teacher_class_dbutil=new teacher_class_dbutil(dataSource);
		}
		catch(Exception exc) {

		throw new ServletException(exc);
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		try {
//			listteacher(request,response);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		try {
			String thecommand=request.getParameter("command");
			if(thecommand==null) {
				thecommand="LIST";
			}
			switch(thecommand) {
			case"LIST":
				listteacher(request,response);
				break;
			case"DELETE":
				deleteteacher(request,response);
				break;
			}
		}
		catch(Exception exc) {
			throw new ServletException(exc);
		}
	}
	private void deleteteacher(HttpServletRequest request, HttpServletResponse response)throws Exception {
		String theteacherid = request.getParameter("teacherId");
		teacher_class_dbutil.delete(theteacherid);
	listteacher(request, response);		
	}
	private void listteacher(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<student> student=teacher_class_dbutil.getstudent();
		request.setAttribute("select",student);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/teacher.jsp");
		dispatcher.forward(request,response);
	}
}
