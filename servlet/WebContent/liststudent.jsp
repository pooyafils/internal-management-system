<%-- <%@ page import="java.util.*" %> --%>
<%-- <%@ page import="com.web.*" %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
	<title>Student Tracker App</title>	
<!-- 	<link type="text/css" rel="stylesheet" href="css/style.css"> -->
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#datatable').DataTable();
	});
	</script>
</head>

<%-- <% --%>
<!-- 	// get the students from the request object (sent by servlet) -->
<%-- 	List<student> theStudents = (List<student>) request.getAttribute("studentlist");%> --%>

<body>
<%
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("pragma","no-cache");
response.setHeader("Expire","0");



response.setHeader("Expires", "0");
if(session.getAttribute("username")==null){
	response.sendRedirect("login.jsp");
}
%>
${username}<br>
<form action="logout">
<input type="submit" value="logout">
</form>
	<div id="wrapper">
		<div id="header">
			<h2>FooBar University</h2>
		</div>
	</div>

	<div id="container">
	
		<div id="content">
		<input type="button" value="Add Student"
		onclick="window.location.href='add-student-fo.jsp';return false;"
		class="add-student-button"
		/>
		
	
	<form action="tavle" method="get">

<input type="submit" value="teacher">

</form>

<br>
			<table id="datatable">
			<thead>
				<tr>
				  
				
					<th class="th-sm" >First Name</th>
					<th class="th-sm">Last Name</th>
					<th class="th-sm">Email</th>
					<th class="th-sm">studentId</th>
					<th class="th-sm">studentClass</th>
					<th class="th-sm">Action</th>
					
					  
					
				</tr>
				</thead>
<%-- 				<% for (student tempStudent : theStudents) { %> --%>
<!-- 					<tr> -->
<%-- 						<td> <%= tempstudent.getEmail() %> </td> --%>
<%-- 						<td> <%= tempstudent.getLastName() %> </td> --%>
<%-- 						<td> <%= tempstudent.getEmail() %> </td> --%>
<!-- 					</tr> -->
<tbody>
				<c:forEach var="tempstudent" items="${studentlist}">
				<c:url var="templink" value="conyrolservlet">
				<c:param name="command" value="LOAD"/>
				<c:param name="studentId" value="${tempstudent.id}"/>
				</c:url>
			<c:url var="delete" value="conyrolservlet">
			<c:param name="command" value="DELETE"/>
			<c:param name="studentId" value="${tempstudent.id}"/>
			
			</c:url>
				  <c:url var="temp" value="conyrolservlet">
				  <c:param name="command" value="PRINT"/>
				  <c:param name="studentIdPrint" value="${tempstudent.id}"/>
				  
				  
				  </c:url>
				
					<tr>
						<td >${tempstudent.firstName}</td>
						<td> ${tempstudent.lastName} </td>
						<td> ${tempstudent.email} </td>
						<td> ${tempstudent.studentId} </td>
						<td> ${tempstudent.studentClass} </td>
						<td><a href="${templink}">Update </a>|
								<a href="${temp}">Print </a>	|
						<a href="${delete}"
						onclick="if (!(confirm('Are you sure you want to delete this student?'))) return false">
						
						DELETE</a>								
								
						</td>
					</tr>
					
					
				</c:forEach>
<%-- 				<% } %> --%>
				  </tbody>
			</table>
		
		</div>
	
	</div>
</body>


</html>


