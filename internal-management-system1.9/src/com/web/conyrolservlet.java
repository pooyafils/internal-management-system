package com.web;
import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
@WebServlet("/conyrolservlet")
public class conyrolservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private dbutil dbutil;
	@Resource(name="jdbc/web_student_tracker")
	private DataSource dataSource;
	@Override
	public void init() throws ServletException {
		//dbutil= new dbutil(dataSource);
		super.init();
		try {
			dbutil=new dbutil(dataSource);
		}
		catch(Exception exc) {
		throw new ServletException(exc);
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String theCommand=request.getParameter("command");
			if(theCommand==null) {
				theCommand="LIST";
			}
			switch(theCommand){
				case"LIST":
			        liststudent(request,response);
			break;
				case"ADD":
				    addStudent(request,response);
				break;
				case"LOAD":
					loadstudent(request,response);
					break;
				case "UPDATE":
					updateStudent(request, response);
					break;
				case "DELETE":
					deletestudent(request, response);
					break;
				case "PRINT":
					printstudent(request,response);
					break;
				default:
					liststudent(request,response);	
			}
		}
		catch(Exception exc) {
			throw new ServletException(exc);
		}
	}
	private void printstudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
			String theStudentId=request.getParameter("studentIdPrint");
			student thestudent =dbutil.getstudent(theStudentId);
			request.setAttribute("thestudent",thestudent);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/printstudent.jsp");
			dispatcher.forward(request,response);
	}
	private void deletestudent(HttpServletRequest request, HttpServletResponse response) throws Exception{
				String theStudentId = request.getParameter("studentId");
					dbutil.deleteStudent(theStudentId);
				liststudent(request, response);
	}
	private void updateStudent(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
			int id = Integer.parseInt(request.getParameter("studentId"));
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			String studentClass=request.getParameter("studentClass");
			int studentId = Integer.parseInt(request.getParameter("studentId"));
			String user=request.getParameter("user");
			String pass=request.getParameter("pass");

			student thestudent = new student(id, firstName, lastName, email,studentId,studentClass,user,pass);
			dbutil.updateStudent(thestudent);
			liststudent(request, response);
		}
	private void loadstudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
			String theStudentId=request.getParameter("studentId");
			student thestudent=dbutil.getstudent(theStudentId);
			request.setAttribute("thestudent", thestudent);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/update-student-for.jsp");
			dispatcher.forward(request,response);
	}
	private void addStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String firstName=request.getParameter("firstName");
		String lastName=request.getParameter("lastName");
		String email=request.getParameter("email");
		String studentClass=request.getParameter("studentClass");
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		student thestudent=new student(firstName,lastName,email,studentId,studentClass); 
		dbutil.addstudent(thestudent);
		liststudent(request,response);
	}
	private void liststudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<student> student=dbutil.getstudent();
	request.setAttribute("studentlist",student);
	RequestDispatcher dispatcher = request.getRequestDispatcher("/liststudent.jsp");
	dispatcher.forward(request,response);
	}

	

}
