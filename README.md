# Internal management system for schools
the internal management system is the application based on java enterprise 7.0
which I used servlet and jsp api for development prosess.this application can be
useful for people who want to start learning servlet and jsp development. this
application support all development process for crud and some additional feature
web application need
## Getting Started
you need to have 
- jDK 8
- RDMS(I recomand you xampp for beginners )
- mysql connector 5.1
- jstl1.2 jar
- eclipse for Enterprise Java Developers 
- tomcat 8.5
### Prerequisites
you need to install eclipse Enterprise version and create dynamic web project 
 on eclipse and download or clone my-System-app folder on your computer.
you need also to install tomcat8.5 on eclipes
### Installing
After creating a dynamic web project on eclipse go to the project folder and
replace a webcontent and src folders with the webcontent and src floders that 
you download or clone (all needed jar fils are located in the webcontent/WEB-INF
so if you replace a downloaded folders with your new dynamic project this is no
problem with jar fils or any other dependencies). you need to run database 
script that is located on sql folder by your RDMS software
## Authors
 **Pooya fils** - *Initial work* - [pooyafils](https://gitlab.com/pooyafils)

